#!/bin/bash

updateLpkg() {
    # sudow=sudow
    bash -c "lpkg update-list" #2> /dev/null 1> /dev/null
}

updateLpkgbuild() {
    bash -c "lpkgbuild update"
}

checkInstalledLpkg() {
    /usr/sbin/lpkg -i | grep "$1"
}

checkInstalledLpkgbuild() {
    # ls /opt/Loc-OS-LPKG/lpkgbuild/remove/ | grep $1
    /usr/sbin/lpkgbuild list | grep "$1"
}

installLpkg() {
    pkg="$1"
    echo "Instalando $pkg..."
    pkexec /usr/sbin/lpkg get-install "$1" 2> /dev/null 1> /dev/null
    # sleep 1
    echo "Verificando instalación"
    if [ "$(checkInstalledLpkg "$pkg")" = "" ]; then
        echo "Error en la instalación, $pkg no instalado"
    else
        echo "$pkg instalado correctamente"
    fi
}

installLpkgbuild() {
    pkgbuild="$1"
    echo "Instalando $pkgbuild..."
    pkexec /usr/sbin/lpkgbuild install "$pkgbuild"
    echo "Verificando instalación..."
    if [ "$(checkInstalledLpkgbuild "$pkgbuild")" = "" ]; then
        echo "Error en la instalación, $pkgbuild no instalado"
    else
        echo "$1 instalado correctamente"
    fi
}

removeLpkg() {
    pkexec /usr/sbin/lpkg remove "$1"
}

removeLpkgbuild() {
    pkexec /usr/sbin/lpkgbuild remove "$1"
}

case "$1" in
   install-lpkg)
       installLpkg "$2";;
   remove-lpkg)
       removeLpkg "$2";;
    update-lpkg)
       updateLpkg ;;
    check-lpkg)
       checkInstalledLpkg "$2";;
    install-lpkgbuild)
        installLpkgbuild "$2";;
    remove-lpkgbuild)
        removeLpkgbuild "$2";;
    update-lpkgbuild)
        updateLpkgbuild ;;
    check-lpkgbuild)
        checkInstalledLpkgbuild "$2";;
   *)
       echo "Command not found:" "$@"
       exit 1;;
esac
