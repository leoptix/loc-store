# loc_store

A store for Loc-OS Linux

<!-- 
Elementos a probar:
ListTile 
DataTable
AlertDialog
Image
LinearProgressIndicator
url_package
animations:OpenContainer
CachedNetworkImage
NavigationRail <---------------
TabBar
ListView(GridView(shrinWrap: true))
-->

## Getting Started

To build run:

```bash
git clone https://https://gitlab.com/loc-os_linux/loc-store loc_store
cd loc_store
flutter create . --platforms=linux
./pkg.sh
```

<!-- This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
-->