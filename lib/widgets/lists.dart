import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:loc_store/backend/getjson.dart';
import 'package:loc_store/backend/getlpkgs.dart';
import 'package:loc_store/backend/getlpkgbuilds.dart';
import 'package:loc_store/pages/lpkg.dart';
import 'package:loc_store/widgets/button_state.dart';
import 'package:loc_store/widgets/title_name.dart';

// Widget lpkgList(BuildContext context, bg, fg, base, success) {
//   return Container(
//     color: bg,
//     alignment: Alignment.center,
//     child: lpkgListRepo(context, bg, fg, base, success),
//   );
// }

// Widget lpkgbuildList(bg, fg, success) {
//   return Container(
//     color: bg,
//     alignment: Alignment.center,
//     child: lpkgbuildListRepo(bg, fg, success),
//   );
// }

Widget lpkgListRepo(
    BuildContext context, Color bg, Color fg, Color base, Color success) {
  return FutureBuilder<LocJson?>(
    future: LpkgRequest().packages,
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const Center(child: CircularProgressIndicator.adaptive());
      }
      final LocJson? lpkg = snapshot.data;
      return ListView.builder(
          itemCount: lpkg?.lpkg.length ?? 0,
          itemBuilder: (context, index) {
            return Container(
              margin: const EdgeInsets.all(10),
              color: bg,
              child: ListTile(
                selectedTileColor: fg,
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => lpkgPage(
                              context,
                              bg,
                              base,
                              fg,
                              lpkg.lpkg[index].name,
                              lpkg.lpkg[index].icon,
                              lpkg.lpkg[index].screenshot,
                              lpkg.lpkg[index].description,
                              lpkg.lpkg[index].package,
                              lpkg.lpkg[index].version,
                              lpkg.lpkg[index].size,
                              lpkg.lpkg[index].dependencies,
                              lpkg.lpkg[index].web,
                              lpkg.lpkg[index].license,
                              lpkg.lpkg[index].mantainer)));
                },
                leading: CachedNetworkImage(
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  imageUrl: lpkg!.lpkg[index].icon,
                  height: 48,
                  width: 48,
                ),
                title: titleName(lpkg.lpkg[index].name,
                    lpkg.lpkg[index].version, bg, fg, success, "lpkg"),
                subtitle: Text(
                  lpkg.lpkg[index].size,
                  style: TextStyle(color: fg),
                ),
                trailing: buttonState(lpkg.lpkg[index].name, bg, fg, "lpkg"),
                isThreeLine: true,
              ),
            );
          });
    },
  );
}

Widget lpkgbuildListRepo(bg, fg, success) {
  return FutureBuilder<LocJson2?>(
    future: LpkgbuildRequest().scripts,
    builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return const Center(child: CircularProgressIndicator.adaptive());
      }
      final LocJson2? lpkgbuild = snapshot.data;
      return ListView.builder(
          itemCount: lpkgbuild?.lpkgbuild.length ?? 0,
          itemBuilder: (context, index) {
            return Container(
              margin: const EdgeInsets.all(10),
              color: bg,
              child: ListTile(
                onTap: () {},
                leading: CachedNetworkImage(
                  // color: fg,
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  imageUrl: lpkgbuild!.lpkgbuild[index].icon,
                  height: 48,
                  width: 48,
                ),
                title: titleName(lpkgbuild.lpkgbuild[index].name, "", bg, fg,
                    success, "lpkgbuild"),
                subtitle: Text(lpkgbuild.lpkgbuild[index].description,
                    style: TextStyle(color: fg)),
                trailing: buttonState(
                    lpkgbuild.lpkgbuild[index].name, bg, fg, "lpkgbuild"),
                isThreeLine: true,
              ),
            );
          });
    },
  );
}
