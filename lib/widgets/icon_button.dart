import 'package:flutter/material.dart';

Widget iconButtonBuilder(context, string, icon, desc, bg, fg, name, function) {
  return IconButton(
      tooltip: string,
      icon: Icon(
        icon,
        color: fg,
      ),
      onPressed: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => function(context, desc, bg, fg, name)));
      });
}
