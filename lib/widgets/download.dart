import 'package:flutter/material.dart';
import 'package:loc_store/backend/manage.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Widget download(context, String desc, Color bg, Color fg, String package) {
  // lpkgAvaible(package);
  Future<int> typeDownload;
  if (desc == "lpkg") {
    // typeDownload = lpkgUpdate();
    typeDownload = lpkgInstall(package);
  } else {
    typeDownload = lpkgbuildInstall(package);
  }
  return AlertDialog(
    backgroundColor: bg,
    title: Text("${AppLocalizations.of(context)?.installing}...",
        style: TextStyle(color: fg)),
    content: FutureBuilder(
      future: typeDownload, // lpkgAvaible(package),
      builder: (context, snapshot) {
        if (snapshot.data == 0) {
          return Text("$package ${AppLocalizations.of(context)?.installed}",
              style: TextStyle(color: fg));
        } else if (snapshot.data == 1) {
          return Text("$package ${AppLocalizations.of(context)?.notinstalled}",
              style: TextStyle(color: fg));
        } else {
          return LinearProgressIndicator(
            color: fg,
            backgroundColor: bg,
          );
        }
      },
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () => Navigator.pop(context, 'OK'),
        child: Text("${AppLocalizations.of(context)?.finish}",
            style: TextStyle(color: fg)),
      ),
    ],
  );
}
