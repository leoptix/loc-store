import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:loc_store/widgets/detail_box.dart';
import 'package:loc_store/widgets/installation_buttons.dart';

Widget lpkgPage(context, bg, base, fg, name, icon, screenshot, description,
    package, version, size, dependencies, web, license, maintainer) {
  var appName = "Loc-Store";

  // Strings of detail section
  List strings = [
    "${AppLocalizations.of(context)?.version}: ",
    "${AppLocalizations.of(context)?.size}: ",
    "${AppLocalizations.of(context)?.homepage}: ",
    "${AppLocalizations.of(context)?.license}: ",
    "${AppLocalizations.of(context)?.mantainer}: ",
    "${AppLocalizations.of(context)?.dependencies}: ",
  ];

  // Details of packages
  List datas = [version, size, web, license, maintainer, dependencies];

  // Check dependencies of package
  if (datas[5] == "") {
    datas.removeAt(5);
    strings.removeAt(5);
  }

  return Scaffold(
      backgroundColor: bg,
      appBar: AppBar(
        backgroundColor: base,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: fg,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Row(children: [
          Text(
            appName,
            style: TextStyle(color: fg),
            textAlign: TextAlign.justify,
          ),
        ]),
      ),
      body: ListView(children: [
        // Header screenshot with name and icon of package
        Stack(
          alignment: AlignmentDirectional.bottomStart,
          children: [
            // Screenshot
            Center(
              child: CachedNetworkImage(
                // color: fg,
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                imageUrl: screenshot,
                cacheKey: name,
                maxWidthDiskCache: 1280,
                maxHeightDiskCache: 720,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      colors: [bg, const Color.fromARGB(0, 0, 0, 0)],
                      stops: const [0.0, 1.0],
                      begin: FractionalOffset.bottomLeft,
                      end: FractionalOffset.centerRight)),
              padding: const EdgeInsets.all(10),
              child: Row(children: [
                // Icon
                CachedNetworkImage(
                  imageUrl: icon,
                  maxWidthDiskCache: 96,
                  maxHeightDiskCache: 96,
                ),
                // Name
                Text(
                  name,
                  style: TextStyle(color: fg, fontSize: 48),
                  textAlign: TextAlign.right,
                ),
              ]),
            )
          ],
        ),

        // Description text
        Container(
          padding: const EdgeInsets.all(10),
          child: Center(
              child: Text(
            description,
            style: TextStyle(color: fg, fontSize: 20),
            textAlign: TextAlign.center,
          )),
        ),

        // Install and uninstall buttons
        Center(
            widthFactor: 50, child: installationButtons(context, bg, fg, name)),

        // Details section
        GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 10),
          shrinkWrap: true,
          itemCount: datas.length,
          itemBuilder: (BuildContext context, int index) {
            return detailBox(strings[index], datas[index], fg);
          },
        ),
      ]));
}
