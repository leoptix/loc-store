#!/bin/bash
NAME=Loc-Store
DIR=opt/Loc-OS-LPKG/loc-store
BUNDLE_DIR=releases/bundle
BUILD_DIR=build/linux/x64/release/bundle
PUB_VER=$(grep version: < pubspec.yaml)
VER_BUILD=$(echo "$PUB_VER" | awk -F' ' '{ print $2 }')
VERSION=$(echo "$VER_BUILD" | awk -F'+' '{ print $1 }')
BUILD=$(echo "$VER_BUILD" | awk -F'+' '{ print $2 }')
SIZE=$(du -sk $BUNDLE_DIR | awk -F' ' '{ print $1 }')

main() {
if ! [ "$(arch)" = "x86_64" ]; then
echo -e "\e[1;31mThis script only works on 64 bit"
exit 1
fi
BUILD_YN=$(echo -e "Do you want build the project?" "\nYes\nNo" | fzf --layout=reverse --border --header-lines=1)
if [ "$BUILD_YN" = "Yes" ]; then
build
fi

LPKG_YN=$(echo -e "Do you want make the .lpkg file?" "\nYes\nNo" | fzf --layout=reverse --border --header-lines=1)
if [ "$LPKG_YN" = "Yes" ]; then
makelpkg
fi

DEB_YN=$(echo -e "Do you want make the .deb file?" "\nYes\nNo" | fzf --layout=reverse --border --header-lines=1)
if [ "$DEB_YN" = "Yes" ]; then
makedeb
fi
}

build() {
flutter gen-l10n || exit
flutter build linux || exit
chmod +x $BUILD_DIR/data/flutter_assets/assets/liblpkg.sh
}

fail() {
rm -r $BUNDLE_DIR/desc $BUNDLE_DIR/DEBIAN
exit 1
}

makelpkg() {
if ! [ "$(which createlpkg)" ]; then
echo -e "\e[1;31mError: createlpkg not found"
exit 1
fi
mkdir -p $BUNDLE_DIR/desc
echo "mantenedor='José Leiva (Anonyzard)'
paquete='$PKG_NAME'
version='$VERSION'
licencia='GPL'
homepage='https://gitlab.com/loc-os_linux/loc-store'" > $BUNDLE_DIR/desc/pkgdesc

PKG_NAME=loc_store
mkdir -p $BUNDLE_DIR/$DIR
cp $BUILD_DIR/* $BUNDLE_DIR/$DIR -rvu
cd $BUNDLE_DIR || exit
rm ../$PKG_NAME*.lpkg
createlpkg -c $PKG_NAME-"$VERSION"-"$BUILD".lpkg
rm desc -r
cd - || exit
}

makedeb() {
PKG_NAME=loc-store
if ! [ "$(which dpkg-deb)" ]; then
echo -e "\e[1;31mError: dpkg-deb not found"
exit 1
fi
mkdir -p $BUNDLE_DIR/DEBIAN
echo "Package: $PKG_NAME
Version: $VERSION-$BUILD
Depends: policykit-1, bash
Section: admin
Priority: optional
Homepage: https://gitlab.com/loc-os_linux/loc-store
Architecture: amd64
Installed-Size: $SIZE
Maintainer: José Leiva <anonyzard@disroot.org>
Description: Store for Loc-OS Linux
 $NAME is a simple interface to install or remove LPKGs and LPKGBUILDs.
 .
 This package requires LPKG and LPKGBUILD installed to be used." > $BUNDLE_DIR/DEBIAN/control

mkdir -p $BUNDLE_DIR/$DIR
cp $BUILD_DIR/* $BUNDLE_DIR/$DIR -rvu
rm releases/*.deb
dpkg-deb --root-owner-group --build $BUNDLE_DIR releases
rm $BUNDLE_DIR/DEBIAN -r
}

main
