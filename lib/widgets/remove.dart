import 'package:flutter/material.dart';
import 'package:loc_store/backend/manage.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Widget remove(context, String desc, Color bg, Color fg, String package) {
  // lpkgAvaible(package);
  Future<int> typeRemove;
  if (desc == "lpkg") {
    typeRemove = lpkgRemove(package);
  } else {
    typeRemove = lpkgbuildRemove(package);
  }
  return AlertDialog(
    backgroundColor: bg,
    title: Text("${AppLocalizations.of(context)?.uninstalling}...",
        style: TextStyle(color: fg)),
    content: FutureBuilder(
      future: typeRemove,
      builder: (context, snapshot) {
        if (snapshot.data == 0) {
          return Text("$package ${AppLocalizations.of(context)?.uninstalled}",
              style: TextStyle(color: fg));
        } else if (snapshot.data == 1) {
          return Text(
              "$package ${AppLocalizations.of(context)?.notuninstalled}",
              style: TextStyle(color: fg));
        } else {
          return LinearProgressIndicator(
            color: fg,
            backgroundColor: bg,
          );
        }
      },
    ),
    actions: <Widget>[
      TextButton(
        onPressed: () => Navigator.pop(context, 'OK'),
        child: Text("${AppLocalizations.of(context)?.finish}",
            style: TextStyle(color: fg)),
      ),
    ],
  );
}
