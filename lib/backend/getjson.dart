import 'package:flutter/foundation.dart';
import 'package:loc_store/backend/getlpkgbuilds.dart';
import 'package:loc_store/backend/getlpkgs.dart';
import 'package:http/http.dart' as http;
import 'package:loc_store/backend/manage.dart';

class LpkgRequest {
  Future<LocJson?> get packages async {
    // var url = Uri.http("127.0.0.1:5000", "/lpkg");
    // var url = Uri.http("github.com", "/Anonyzard/Anonyzard/raw/main/lpkg.json");
    var url =
        Uri.http("gitlab.com", "/locosporlinux/lpkg-list/-/raw/main/lpkg.json");
    final http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      if (kDebugMode) {
        print(response.body);
      }
      return locJsonFromJson(response.body);
    }
    return null;
  }
}

class LpkgbuildRequest {
  Future<LocJson2?> get scripts async {
    // var url = Uri.http("127.0.0.1:5000", "/lpkgbuild");
    // var url = Uri.http("github.com", "/Anonyzard/Anonyzard/raw/main/lpkgbuild.json");
    var url = Uri.http(
        "gitlab.com", "/locosporlinux/lpkg-list/-/raw/main/lpkgbuild.json");
    final http.Response response = await http.get(url);
    lpkgbuildUpdate();

    if (response.statusCode == 200) {
      if (kDebugMode) {
        print(response.body);
      }
      return locJson2FromJson(response.body);
    }
    return null;
  }
}
