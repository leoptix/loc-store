// To parse this JSON data, do
//
//     final locJson = locJsonFromJson(jsonString);

import 'dart:convert';

LocJson locJsonFromJson(String str) => LocJson.fromJson(json.decode(str));

String locJsonToJson(LocJson data) => json.encode(data.toJson());

class LocJson {
  LocJson({
    required this.lpkg,
  });

  List<Lpkg> lpkg;

  factory LocJson.fromJson(Map<String, dynamic> json) => LocJson(
        lpkg: List<Lpkg>.from(json["lpkg"].map((x) => Lpkg.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "lpkg": List<dynamic>.from(lpkg.map((x) => x.toJson())),
      };
}

class Lpkg {
  Lpkg(
      {required this.name,
      required this.package,
      required this.version,
      required this.icon,
      required this.size,
      required this.description,
      required this.web,
      required this.mantainer,
      required this.license,
      required this.screenshot,
      required this.dependencies});

  String name;
  String package;
  String version;
  String icon;
  String size;
  String description;
  String web;
  String mantainer;
  String license;
  // bool remote;
  String screenshot;
  String dependencies;

  factory Lpkg.fromJson(Map<String, dynamic> json) => Lpkg(
      name: json["name"],
      package: json["package"],
      version: json["version"],
      icon: json["icon"],
      size: json["size"],
      description: json["description"],
      web: json["web"],
      mantainer: json["mantainer"],
      license: json["license"],
      screenshot: json["screenshot"],
      dependencies: json["dependencies"]);

  Map<String, dynamic> toJson() => {
        "name": name,
        "package": package,
        "version": version,
        "icon": icon,
        "size": size,
        "description": description,
        "web": web,
        "mantainer": mantainer,
        "license": license,
        "screenshot": screenshot,
        "dependencies": dependencies,
      };
}
