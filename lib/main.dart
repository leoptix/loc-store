import 'package:flutter/widgets.dart';
import 'package:loc_store/pages/main_screen.dart';

void main() {
  runApp(const MyStore());
}
/* 
backend: Calls to system and conections to repo
l10n: Localization files (edit manually l10n.dart to add new languages)
pages: Structure of pages
widgets: Elements of pages
*/
