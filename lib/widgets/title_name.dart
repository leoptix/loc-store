import 'package:flutter/material.dart';
import 'package:loc_store/backend/manage.dart';

Widget titleName(name, version, bg, fg, success, desc) {
  Future<bool> typeAvaible;
  if (desc == "lpkg") {
    typeAvaible = lpkgAvaible(name);
  } else {
    typeAvaible = lpkgbuildAvaible(name);
  }
  return FutureBuilder(
    future: typeAvaible,
    builder: (context, snapshot) {
      if (snapshot.data == true) {
        return Text(
          "$name $version",
          style: TextStyle(color: success),
        );
      } else if (snapshot.data == false) {
        return Text(
          "$name $version",
          style: TextStyle(color: fg),
        );
      } else {
        return LinearProgressIndicator(
          color: fg,
          backgroundColor: bg,
        );
      }
    },
  );
}
