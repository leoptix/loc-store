import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

Widget detailBox(name, content, fg) {
  if (kDebugMode) {
    if ("http*" == content) {
      print("$content it's a link");
    }
  }
  return Container(
    color: const Color.fromARGB(0, 0, 0, 0),
    child: Column(children: [
      Text(
        name,
        style: TextStyle(color: fg, fontSize: 16),
        textAlign: TextAlign.left,
      ),
      SelectableText(
        content,
        style: TextStyle(color: fg, fontSize: 12),
      ),
    ]),
  );
}
