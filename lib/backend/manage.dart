// ignore_for_file: unused_local_variable

import 'dart:io';
import 'package:flutter/foundation.dart';

void main() {}

// String liblpkg = "assets/liblpkg.sh";
String liblpkg =
    "/opt/Loc-OS-LPKG/loc-store/data/flutter_assets/assets/liblpkg.sh";

String appdir = "/home/anonyzard/.git/loc_store";

ProcessResult chmodsh = Process.runSync("chmod", ["+x", "assets/liblpkg.sh"]);

Future<int> lpkgUpdate() async {
  ProcessResult result = await Process.run(liblpkg, ["update-lpkg"]);
  // if (kDebugMode) {
  //   print(result.stdout);
  // }
  return 0;
}

Future<bool> lpkgAvaible(String package) async {
  ProcessResult result = await Process.run(liblpkg, ["check-lpkg", package]);
  String togrep = result.stdout;
  if (kDebugMode) {
    print("stdout");
    print(result.stdout);
    print("stderr");
    print(result.stderr);
  }

  if (await result.stdout != "") {
    List<String> elements = togrep.split("-");
    String version = elements[1];
    String build = elements[2];
    if (kDebugMode) {
      print(
          "$package está instalado en su versión $version y número de compilación $build");
    }
    return true;
  } else {
    if (kDebugMode) {
      print("lpkg: $package no está instalado");
    }
  }
  return false;
}

// int lpkgAvaibleInternal(String package) {
//   ProcessResult result = Process.runSync(liblpkg, ["check-lpkg", package]);
//   String togrep = result.stdout;
//   if (kDebugMode) {
//     print("stdout");
//     print(result.stdout);
//     print("stderr");
//     print(result.stderr);
//   }

//   if (result.stdout == "") {
//     if (kDebugMode) {
//       print("lpkg: $package no está instalado");
//     }
//     return 1;
//   } else {
//     List<String> elements = togrep.split("-");
//     String version = elements[1];
//     String build = elements[2];
//     if (kDebugMode) {
//       print(
//           "$package está instalado en su versión $version y número de compilación $build");
//     }
//     return 0;
//   }
// }

Future<int> lpkgInstall(String package) async {
  lpkgUpdate();
  // ProcessResult update = await Process.run(liblpkg, ["update-lpkg"]);
  ProcessResult installation =
      await Process.run(liblpkg, ["install-lpkg", package]);
  // lpkgUpdate();
  Future<bool> result = lpkgAvaible(package);
  if (await result) {
    if (kDebugMode) {
      print("$package se instaló correctamente");
    }
    return 0;
  } else {
    if (kDebugMode) {
      print("$package no se instaló correctamente");
    }
    return 1;
  }
}

Future<int> lpkgRemove(String package) async {
  ProcessResult installation =
      await Process.run(liblpkg, ["remove-lpkg", package]);
  // lpkgUpdate();
  Future<bool> result = lpkgAvaible(package);
  if (kDebugMode) {
    print(installation.stdout);
  }
  if (await result == false) {
    if (kDebugMode) {
      print("$package se eliminó correctamente");
    }
    return 0;
  } else {
    if (kDebugMode) {
      print("$package no se eliminó correctamente");
    }
    return 1;
  }
}

void lpkgbuildUpdate() async {
  ProcessResult result = await Process.run(liblpkg, ["update-lpkgbuild"]);
  if (kDebugMode) {
    print(stdout);
  }
}

Future<bool> lpkgbuildAvaible(String package) async {
  ProcessResult result =
      await Process.run(liblpkg, ["check-lpkgbuild", package]);
  if (result.stdout != "") {
    if (kDebugMode) {
      print("$package está instalado");
    }
    return true;
  } else {
    if (kDebugMode) {
      print("lpkgbuild: $package no está instalado");
    }
    return false;
  }
}

// int lpkgbuildAvaibleInternal(String package) {
//   ProcessResult result = Process.runSync(liblpkg, ["check-lpkgbuild", package]);
//   if (result.stdout == "") {
//     if (kDebugMode) {
//       print("lpkgbuild: $package no está instalado");
//     }
//     return 1;
//   } else {
//     if (kDebugMode) {
//       print("$package está instalado");
//     }
//     return 0;
//   }
// }

Future<int> lpkgbuildInstall(String package) async {
  lpkgbuildUpdate();
  ProcessResult installation =
      await Process.run(liblpkg, ["install-lpkgbuild", package]);
  Future<bool> result = lpkgbuildAvaible(package);
  if (await result) {
    if (kDebugMode) {
      print("$package está instalado");
    }
    return 0;
  } else {
    if (kDebugMode) {
      print("$package no está instalado");
    }
    return 1;
  }
}

Future<int> lpkgbuildRemove(String package) async {
  ProcessResult installation =
      await Process.run(liblpkg, ["remove-lpkgbuild", package]);
  Future<bool> result = lpkgbuildAvaible(package);
  if (kDebugMode) {
    print(installation.stdout);
  }
  if (await result == false) {
    if (kDebugMode) {
      print("$package se eliminó correctamente");
    }
    return 0;
  } else {
    if (kDebugMode) {
      print("$package no se eliminó correctamente");
    }
    return 1;
  }
}
