import 'dart:io';

import 'package:date_time/date_time.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

Widget dynamicAppLogo() {
  int trys = 0;
  var button = IconButton(
    icon: const Icon(
      Icons.games,
      size: 0,
    ),
    onPressed: () {
      if (trys == 22) {
        Process.run("/bin/x-terminal-emulator", [
          "-e",
          "/opt/Loc-OS-LPKG/loc-store/data/flutter_assets/shaders/easter.egg"
        ]);
        trys = 0;
      } else {
        if (kDebugMode) {
          print(trys);
        }
        trys++;
      }
    },
    highlightColor: const Color.fromARGB(0, 0, 0, 0),
    splashColor: const Color.fromARGB(0, 0, 0, 0),
  );
  var applogo = Image.asset("assets/loc-store.png");
  var now = Date.now().toString();
  List dateNow = now.split("/");
  // [month, day, year]
  if (kDebugMode) {
    print(dateNow);
  }
  if (dateNow[0] == "12" &&
      int.parse(dateNow[1]) > 8 &&
      int.parse(dateNow[1]) < 30) {
    if (kDebugMode) {
      print("navidad");
    }
    return Stack(alignment: AlignmentDirectional.topStart, children: [
      applogo,
      Image.asset(
        "assets/gorro.png",
        width: 20,
        height: 20,
      ),
      button
    ]);
  } else {
    if (kDebugMode) {
      print("else");
    }
    return Stack(
        alignment: AlignmentDirectional.topStart, children: [applogo, button]);
  }
}
