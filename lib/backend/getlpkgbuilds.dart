// To parse this JSON data, do
//
//     final locJson = locJsonFromJson(jsonString);

import 'dart:convert';

LocJson2 locJson2FromJson(String str) => LocJson2.fromJson(json.decode(str));

String locJson2ToJson(LocJson2 data) => json.encode(data.toJson());

class LocJson2 {
  LocJson2({
    required this.lpkgbuild,
  });

  List<Lpkgbuild> lpkgbuild;

  factory LocJson2.fromJson(Map<String, dynamic> json) => LocJson2(
        lpkgbuild: List<Lpkgbuild>.from(
            json["lpkgbuild"].map((x) => Lpkgbuild.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "lpkgbuild": List<dynamic>.from(lpkgbuild.map((x) => x.toJson())),
      };
}

class Lpkgbuild {
  Lpkgbuild({
    required this.name,
    required this.script,
    // required this.version,
    required this.icon,
    // required this.size,
    required this.description,
    // required this.web,
    // required this.mantainer,
    // required this.license,
    // required this.remote,
    // required this.dependencies
  });

  String name;
  String script;
  // String version;
  String icon;
  // String size;
  String description;
  // String web;
  // String mantainer;
  // String license;
  // bool remote;
  // String dependencies;

  factory Lpkgbuild.fromJson(Map<String, dynamic> json) => Lpkgbuild(
        name: json["name"],
        script: json["script"],
        // version: json["version"],
        icon: json["icon"],
        // size: json["size"],
        description: json["description"],
        // web: json["web"],
        // mantainer: json["mantainer"],
        // license: json["license"],
        // remote: json["remote"],
        // dependencies: json["dependencies"]
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "script": script,
        // "version": version,
        "icon": icon,
        // "size": size,
        "description": description,
        // "web": web,
        // "mantainer": mantainer,
        // "license": license,
        // "remote": remote,
        // "dependencies": dependencies,
      };
}
