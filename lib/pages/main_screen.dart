import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gtk_theme_fl/gtk_theme_fl.dart';
import 'package:loc_store/icon_loc_os_icons.dart';
import 'package:loc_store/l10n/l10n.dart';
import 'package:loc_store/widgets/app_logo.dart';
import 'package:loc_store/widgets/lists.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MyStore extends StatefulWidget {
  const MyStore({Key? key}) : super(key: key);
  @override
  State<MyStore> createState() => _MyStoreState();
}

class _MyStoreState extends State<MyStore> {
  // Inicialize GTK Theme Data
  GtkThemeData themeData = GtkThemeData(name: "Default");
  @override
  void initState() {
    initPlatformState();
    super.initState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    themeData = await GtkThemeData.initialize();
    setState(() {});
  }

  // Set some variables for Navigation Rail
  int _selectedIndex = 0;
  NavigationRailLabelType labelType = NavigationRailLabelType.selected;
  bool showLeading = false; // Loc-Store
  bool showTrailing = false;
  double groupAligment = -1.0;

  @override
  Widget build(BuildContext context) {
    // A list with the contents of Navigation Rail
    // final List<Widget> mainContents = [
    //   lpkgList(
    //       context,
    //       Color(themeData.theme_bg_color),
    //       Color(themeData.theme_fg_color),
    //       Color(themeData.theme_base_color),
    //       Color(themeData.success_color)),
    //   lpkgbuildList(Color(themeData.theme_bg_color),
    //       Color(themeData.theme_fg_color), Color(themeData.success_color))
    // ];
    var appName = "Loc-Store";
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: appName,
        supportedLocales: L10n.all,
        localizationsDelegates: const [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate
        ],
        home: Scaffold(
            backgroundColor: Color(themeData.theme_bg_color),
            appBar: AppBar(
              leading: dynamicAppLogo(),
              title: Text(appName,
                  style: TextStyle(color: Color(themeData.theme_fg_color))),
              backgroundColor: Color(themeData.theme_base_color),
            ),
            body: Row(mainAxisSize: MainAxisSize.max, children: [
              NavigationRail(
                selectedIndex: _selectedIndex,
                groupAlignment: groupAligment,
                backgroundColor: Color(themeData.theme_base_color),
                onDestinationSelected: (int index) {
                  setState(() {
                    _selectedIndex = index;
                  });
                },
                labelType: labelType,
                selectedLabelTextStyle:
                    TextStyle(color: Color(themeData.theme_fg_color)),
                unselectedLabelTextStyle:
                    TextStyle(color: Color(themeData.theme_base_color)),
                destinations: [
                  navRailDest(IconLocOS.package, 'LPKG'),
                  navRailDest(IconLocOS.file_code, 'LPKGBUILD')
                  // navRailDest(Icons.settings, 'Settings?'),
                ],
              ),
              loadData(context, _selectedIndex),
            ])));
  }

  Expanded loadData(BuildContext context, int selectedIndex) {
    Widget content;
    if (selectedIndex == 0) {
      content = lpkgListRepo(
          context,
          Color(themeData.theme_bg_color),
          Color(themeData.theme_fg_color),
          Color(themeData.theme_base_color),
          Color(themeData.success_color));
    } else {
      content = lpkgbuildListRepo(Color(themeData.theme_bg_color),
          Color(themeData.theme_fg_color), Color(themeData.success_color));
    }
    return Expanded(child: content);
  }

  NavigationRailDestination navRailDest(IconData icon, String text) {
    return NavigationRailDestination(
        icon: Icon(
          icon,
          color: Color(themeData.theme_fg_color),
        ),
        label: Text(text));
  }
}
