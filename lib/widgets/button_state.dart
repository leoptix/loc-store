import 'package:flutter/material.dart';
import 'package:loc_store/backend/manage.dart';
import 'package:loc_store/widgets/download.dart';
import 'package:loc_store/widgets/icon_button.dart';
import 'package:loc_store/widgets/remove.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Widget buttonState(name, bg, fg, desc) {
  Future<bool> typeAvaible;
  if (desc == "lpkg") {
    typeAvaible = lpkgAvaible(name);
  } else {
    typeAvaible = lpkgbuildAvaible(name);
  }
  return FutureBuilder(
    future: typeAvaible,
    builder: (context, snapshot) {
      // If package is installed
      if (snapshot.data == true) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            iconButtonBuilder(context, AppLocalizations.of(context)?.install,
                Icons.download_rounded, desc, bg, fg, name, download),
            iconButtonBuilder(context, AppLocalizations.of(context)?.uninstall,
                Icons.delete_forever_rounded, desc, bg, fg, name, remove),
          ],
        );
        // If package isn't installed
      } else if (snapshot.data == false) {
        return Row(mainAxisSize: MainAxisSize.min, children: [
          iconButtonBuilder(context, AppLocalizations.of(context)?.install,
              Icons.download_rounded, desc, bg, fg, name, download),
        ]);
        // Loading animation
      } else {
        return CircularProgressIndicator(
          color: fg,
          backgroundColor: bg,
        );
      }
    },
  );
}
