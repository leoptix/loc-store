import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:loc_store/backend/manage.dart';
import 'package:loc_store/widgets/download.dart';
import 'package:loc_store/widgets/remove.dart';

Widget installationButtons(context, bg, fg, name) {
  return FutureBuilder(
    future: lpkgAvaible(name), // lpkgAvaible(package),
    builder: (context, snapshot) {
      if (snapshot.data == true) {
        return Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            MaterialButton(
                color: fg,
                child: Text(
                  "${AppLocalizations.of(context)?.install}",
                  style: TextStyle(color: bg),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              download(context, "lpkg", bg, fg, name)));
                }),
            const Padding(
              padding: EdgeInsets.all(10),
            ),
            MaterialButton(
                color: fg,
                child: Text(
                  "${AppLocalizations.of(context)?.uninstall}",
                  style: TextStyle(color: bg),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              remove(context, "lpkg", bg, fg, name)));
                }),
          ],
        );
      } else if (snapshot.data == false) {
        return MaterialButton(
            color: fg,
            child: Text(
              "${AppLocalizations.of(context)?.install}",
              style: TextStyle(color: bg),
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          download(context, "lpkg", bg, fg, name)));
            });
      } else {
        return LinearProgressIndicator(
          color: fg,
          backgroundColor: bg,
        );
      }
    },
  );
}
